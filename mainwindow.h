#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "gameoflife.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_cbSize_currentIndexChanged(int index);

    void on_pbPlayPause_clicked();

    void play_animation();

    void on_pbNextStep_clicked();

    void on_pbLoadMap_clicked();

    void on_pbSaveMap_clicked();

private:
    Ui::MainWindow *ui;
    QTimer *timer;

    void set_map();

protected:
    GameOfLife *game;
};

#endif // MAINWINDOW_H
