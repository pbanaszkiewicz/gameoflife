#ifndef CELLULARAUTOMATAITEM_H
#define CELLULARAUTOMATAITEM_H

#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>

class CellularAutomataItem : public QGraphicsRectItem
{
public:
    CellularAutomataItem(qreal x, qreal y, qreal width, qreal height) :
        QGraphicsRectItem(x, y, width, height),
        _state(0),
        _neighbors(0)
        {}

    void setState(uint s) { _state = s; }
    uint state() { return _state; }

    void setNeighbors(uint n) { _neighbors = n; }
    uint neighbors() { return _neighbors; }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *options, QWidget *widget);

protected:
    uint _state;
    uint _neighbors;
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

#endif // CELLULARAUTOMATAITEM_H
