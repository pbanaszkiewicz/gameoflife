#ifndef GAMEOFLIFE_H
#define GAMEOFLIFE_H

#include <cellularautomataitem.h>
#include <cmath>

typedef unsigned int uint;

typedef std::vector< std::vector<uint> > map_vector;
typedef std::vector< std::vector<CellularAutomataItem *> > map_item;

class GameOfLife
{
public:
    GameOfLife();
    ~GameOfLife();

    void setSize(uint w, uint h);
    uint width() { return _width; }
    uint height() { return _height; }

    uint counter() { return _counter; }
    void reset();
    void nextGeneration();
    void setItem(uint x, uint y, CellularAutomataItem *item)
    {
        map->at(x).at(y) = item;
    }

    void setState(uint x, uint y, uint state);
    uint state(uint x, uint y) { return map->at(x).at(y)->state(); }

protected:
    uint _counter;
    uint _width;
    uint _height;

    map_item *map;

    uint sumNeighborhood(int x, int y, bool moore);

    uint applyRules(uint currentState, uint neighbors);

    // modulo operand in C++ sucks for negative numbers. Here's proper reimplementation
    int _mod(int a, int b) { return (a >= 0) ? a % b : a % b + std::abs(b); }
};

#endif // GAMEOFLIFE_H
