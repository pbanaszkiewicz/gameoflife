#include <vector>
#include <QDebug>
#include "gameoflife.h"


GameOfLife::GameOfLife() :
    _counter(0),
    _width(0),
    _height(0),
    map(0)
{
}

GameOfLife::~GameOfLife()
{
    delete map;
}

void GameOfLife::setSize(uint w, uint h)
{
    this->_width = w;
    this->_height = h;
    this->_counter = 0;

    this->map = new map_item(_width, std::vector<CellularAutomataItem *>(_height, 0));
}

void GameOfLife::reset()
{
    this->_width = 0;
    this->_height = 0;
    this->_counter = 0;

    delete map;
}

uint GameOfLife::sumNeighborhood(int x, int y, bool moore=true)
{
    uint sum = 0;
    int w = _width;
    int h = _height;

    // first count von Neumann's neightborhood, ie. North, South, West, East
    sum += map->at(x).at(_mod(y-1, h))->state() + map->at(x).at(_mod(y+1, h))->state();
    sum += map->at(_mod(x-1, w)).at(y)->state() + map->at(_mod(x+1, w)).at(y)->state();

    // now count in Moore's neightborhood (NW, SW, NE, SE), if so desired
    if (moore)
    {
        sum += map->at(_mod(x-1, w)).at(_mod(y-1, h))->state() + map->at(_mod(x-1, w)).at(_mod(y+1, h))->state();
        sum += map->at(_mod(x+1, w)).at(_mod(y-1, h))->state() + map->at(_mod(x+1, w)).at(_mod(y+1, h))->state();
    }

    map->at(x).at(y)->setNeighbors(sum);

    return sum;
}

void GameOfLife::nextGeneration()
{
    map_vector *map_copy = new map_vector(_width, std::vector<uint>(_height, 0));

    // we need to work on a copy of current data. Thus two for-loops.
    for (uint x = 0; x < _width; x++)
        for (uint y = 0; y < _height; y++)
            map_copy->at(x).at(y) = applyRules(map->at(x).at(y)->state(), sumNeighborhood(x, y, true));

    for (uint x = 0; x < _width; x++)
        for (uint y = 0; y < _height; y++)
            map->at(x).at(y)->setState(map_copy->at(x).at(y));

    delete map_copy;
    ++_counter;
}

uint GameOfLife::applyRules(uint currentState, uint neighbors)
{
    if (currentState != 0)  // the cell is alive
    {
        if (neighbors < 2)  // the cell dies from loneliness
            return 0;
        else if (neighbors <= 3)  // the cell survives and thrives
            return 1;
        else                // the cell dies from overcrowd
            return 0;
    }
    else  // the cell is dead
    {
        if (neighbors == 3) // new live is born
            return 1;
    }

    return 0;
}

void GameOfLife::setState(uint x, uint y, uint state)
{
    map->at(x).at(y)->setState(state);
    map->at(x).at(y)->update();
}
