#include "cellularautomataitem.h"
#include <QDebug>
#include <QPainter>
#include <QBrush>
#include <QString>
#include <QStyleOptionGraphicsItem>

//void CellularAutomataItem::painEvent()
void CellularAutomataItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *options, QWidget *widget)
{
    QBrush background(_state ? Qt::black : Qt::white);
    painter->drawRect(this->boundingRect());
    painter->fillRect(this->rect(), background);
//    painter->drawText(this->rect(), Qt::AlignHCenter | Qt::AlignVCenter, QString::number(_neighbors));
}

void CellularAutomataItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    setState(!state());
    this->update();
}
