#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "cellularautomataitem.h"
#include <QTimer>
#include <QDebug>
#include <QFileDialog>
#include <cmath>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    game = new GameOfLife();
    ui->setupUi(this);
    this->set_map();

    // create a new timer and connect it's timeout() signal to our play_animation() slot
    this->timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(play_animation()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete game;
}

void MainWindow::set_map()
{
    this->on_cbSize_currentIndexChanged(ui->cbSize->currentIndex());

    // clear counter and status bar message
    ui->statusBar->showMessage(QString("Current step (generation): " + QString::number(this->game->counter())));
}

void MainWindow::on_cbSize_currentIndexChanged(int index)
{
    // create new scene
    QGraphicsScene *scene = new QGraphicsScene;

    int number_x = 0;
    int number_y = 0;
    int size_x = 0;
    int size_y = 0;

    switch (index)
    {
    case 1:
        // size 20x20
        number_x = number_y = 20;
        size_x = size_y = 40;
        break;

    case 2:
        // size 80x22
        number_x = 80;
        number_y = 22;
        size_x = size_y = 10;
        break;

    case 3:
        // size 50x50
        number_x = number_y = 50;
        size_x = size_y = 14;
        break;

    default:
        // size 10x10
        number_x = number_y = 10;
        size_x = size_y = 50;
        break;
    }

    // create the same size game map
    this->game->setSize(number_x, number_y);

    // paint out the grid
    for (int x = 0; x < number_x; x++)
        for (int y = 0; y < number_y; y++)
        {
            CellularAutomataItem *item = new CellularAutomataItem(x * size_x, y * size_y, size_x, size_y);
            scene->addItem(item);

            game->setItem(x, y, item);
        }

    // render scene within view
    ui->graphicsView->setScene(scene);
    ui->graphicsView->show();
}

void MainWindow::play_animation()
{
//    qDebug() << "Timeout!";
    game->nextGeneration();
    ui->graphicsView->scene()->update();
    ui->statusBar->showMessage(QString("Current step (generation): ") + QString::number(game->counter()));
}

void MainWindow::on_pbPlayPause_clicked()
{
    if (timer->isActive())
    {
        timer->stop();

        ui->pbPlayPause->setText(QString("Play!"));
        ui->pbLoadMap->setEnabled(true);
        ui->pbNextStep->setEnabled(true);
        ui->pbSaveMap->setEnabled(true);
        ui->sbSpeed->setEnabled(true);
        ui->cbSize->setEnabled(true);
        ui->graphicsView->setInteractive(true);
    }
    else
    {
        // we set up timer for a 1/Nth of a second (timer takes miliseconds, so we multiply by 1000)
        timer->start(1000. * (1. / ui->sbSpeed->value()));

        ui->pbPlayPause->setText(QString("Pause"));
        ui->pbLoadMap->setEnabled(false);
        ui->pbNextStep->setEnabled(false);
        ui->pbSaveMap->setEnabled(false);
        ui->sbSpeed->setEnabled(false);
        ui->cbSize->setEnabled(false);
        ui->graphicsView->setInteractive(false);
    }
}

void MainWindow::on_pbNextStep_clicked()
{
    play_animation();
}

void MainWindow::on_pbLoadMap_clicked()
{
    // select file
    QString filename = QFileDialog::getOpenFileName(this, "Select file containing map", ".", "Map files (*.map)");

    // read it's contents
    QFile file(filename);
    file.open(QIODevice::ReadOnly);

    uint y = 0;
    while (!file.atEnd())
    {
        QByteArray line = file.readLine();
        for (uint x = 0; x < game->width(); x++)
        {
            game->setState(x, y, QString(QChar(line.at(x))).toUInt());
        }
        ++y;
    }

    file.close();
}

void MainWindow::on_pbSaveMap_clicked()
{
    // select file
    QString filename = QFileDialog::getSaveFileName(this, "Select file to save", ".", "Map files (*.map)");

    // read it's contents
    QFile file(filename);
    file.open(QIODevice::WriteOnly);

    QTextStream content(&file);
    for (uint y = 0; y < game->height(); y++)
    {
        for (uint x = 0; x < game->width(); x++)
        {
            content << game->state(x, y);
        }
        content << "\n";
    }

    file.close();
}
