#-------------------------------------------------
#
# Project created by QtCreator 2013-05-21T21:59:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gameoflife
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cellularautomataitem.cpp \
    gameoflife.cpp

HEADERS  += mainwindow.h \
    cellularautomataitem.h \
    gameoflife.h

FORMS    += mainwindow.ui
